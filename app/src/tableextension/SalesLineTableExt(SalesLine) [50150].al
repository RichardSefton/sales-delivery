tableextension 50150 "Sales Line Table Ext" extends "Sales Line"
{
    trigger OnAfterModify()
    begin
        checkPricesForDelivery();
    end;

    local procedure checkPricesForDelivery()
    var
        salesLine: Record "Sales Line";
        orderTotal: Decimal;
        deliveryExists: Boolean;
        deliveryLine: Integer;
    begin
        deliveryExists := false;
        salesLine.Reset();
        salesLine.SetRange("Document No.", Rec."Document No.");
        if salesLine.FindFirst() then
            repeat
                orderTotal += salesLine."Line Amount";
                if (salesLine.Description = 'Delivery Surcharge') and (salesLine."Type" = salesLine."Type"::Item) then begin
                    deliveryExists := true;
                    deliveryLine := salesLine."Line No.";
                end;
            until salesLine.Next() = 0;

        if (orderTotal < 200) and not deliveryExists then begin
            insertDeliveryLine();
        end else
            if (orderTotal < 200) and deliveryExists then begin
                updateDeliveryLine(deliveryLine);
            end else
                if (orderTotal > 200) and deliveryExists then begin
                    removeDeliveryLine(deliveryLine);
                end;
    end;

    local procedure insertDeliveryLine()
    var
        salesLine: Record "Sales Line";
        nextLineNo: Integer;
    begin
        salesLine.SetRange("Document No.", Rec."Document No.");
        if salesLine.FindLast() then nextLineNo := salesLine."Line No." + 10000;
        salesLine.Init();
        salesLine.Validate("Type", salesLine."Type"::Item);
        salesLine.Validate("Document No.", Rec."Document No.");
        salesLine.Validate("Line No.", nextLineNo);
        salesLine.Validate("No.", 'DEL-1000');
        salesLine.Validate(Quantity, 1);
        salesLine.Insert(true);
    end;

    local procedure updateDeliveryLine(lineNo: Integer)
    var
        salesLine: Record "Sales Line";
    begin
        salesLine.Reset();
        salesLine.SetRange("Document No.", Rec."Document No.");
        salesLine.SetRange("Line No.", lineNo);
        if salesLine.FindFirst() then begin
            //if the delivery in >= 10 then the condition is fulfilled
            if salesLine."Line Amount" < 10.00 then begin
                //adding 10 to the delivery charge incase its been set for other reasons
                salesLine.Validate("Line Amount", salesLine."Line Amount" + 10.00);
                //just check theres only one of these. 
                salesLine.Validate(Quantity, 1);
                salesLine.Modify(true);
            end
        end;
    end;

    local procedure removeDeliveryLine(lineNo: Integer)
    var
        salesLine: Record "Sales Line";
    begin
        //if it equals 10 remove the line entirely
        //if its more than 10 subtract 10
        salesLine.Reset();
        salesLine.SetRange("Document No.", Rec."Document No.");
        salesLine.SetRange("Line No.", lineNo);
        if salesLine.FindFirst() then begin
            if salesLine."Line Amount" = 10.00 then begin
                salesLine.Delete(true);
            end else
                if salesLine."Line Amount" > 10.00 then begin
                    salesLine.Validate("Line Amount", salesLine."Line Amount" - 10.00);
                    //its removing so dont care about the qty..
                    salesLine.Modify(true);
                end;
        end;
        //otherwise (less than 10) assume its been added for other reasons 
    end;
}