codeunit 50160 "Sales Delivery Tests"
{
    Subtype = Test;

    [Test]
    procedure DeliveryLineInserts()
    var
        SalesOrder: TestPage "Sales Order";
    begin
        SalesOrder.OpenNew();
        newSalesOrder(SalesOrder, true);
        testDeliveryLineExists(SalesOrder, true);
    end;

    [Test]
    procedure DeliveryLineUpdates()
    var
        SalesOrder: TestPage "Sales Order";

    begin
        SalesOrder.OpenNew();
        newSalesOrder(SalesOrder, true);
        testDeliveryLineExists(SalesOrder, true);
        updateQuantity(SalesOrder, true);
        testDeliveryLineExists(SalesOrder, true);
    end;

    [Test]
    procedure DeliveryLineRemoves()
    var
        SalesOrder: TestPage "Sales Order";
        Item: Record Item;
    begin
        SalesOrder.OpenNew();
        newSalesOrder(SalesOrder, true);
        testDeliveryLineExists(SalesOrder, true);
        updateQuantity(SalesOrder, false);
        testDeliveryLineExists(SalesOrder, false);
    end;

    local procedure newSalesOrder(SalesOrder: TestPage "Sales Order"; lessThan200: Boolean)
    var
        LibrarySales: Codeunit "Library - Sales";

    begin
        CreateTempData();
        SalesOrder."Sell-to Customer No.".Value := SalesOrderSellToCustomerNo;
        SalesOrder.SalesLines."No.".Value := SalesLineItemNo;
        if lessThan200 then begin
            SalesOrder.SalesLines.Quantity.Value := SalesLineQty1;
        end else begin
            SalesOrder.SalesLines.Quantity.Value := SalesLineQty2;
        end;
    end;

    local procedure CreateTempData(): Record Customer
    var
        LibrarySales: Codeunit "Library - Sales";
        LibraryInventory: Codeunit "Library - Inventory";
        Item: Record Item;
    begin
        SalesOrderSellToCustomerNo := LibrarySales.CreateCustomerNo();
        SalesLineItemNo := LibraryInventory.CreateItemNo();
        Item.Reset();
        if Item.get(SalesLineItemNo) then begin
            //LibraryInventory.CreateItemWithUnitPriceAndUnitCost(Item, 20.00, 10.00); //can't use as it creates a new item not update existing
            Item.Validate("Unit Price", 20.00);
            Item.Modify(true);
        end;
        Item.Reset();
        Item.SetRange("No.", DeliveryLineNo);
        if not Item.FindLast() then begin
            LibraryInventory.CreateItemWithUnitPriceAndUnitCost(Item, 10.00, 5.00);
            Item.Rename(DeliveryLineNo);
            Item.Validate(Description, DeliveryLineDescription);
            Item.Modify(true);
        end;
    end;

    local procedure testDeliveryLineExists(SalesOrder: TestPage "Sales Order"; lessThan200: Boolean)
    begin
        SalesOrder.SalesLines.Last();
        if lessThan200 then begin
            Assert.AreEqual(DeliveryLineNo, SalesOrder.SalesLines."No.".Value, 'Delivery Line No. is correct');
            Assert.AreEqual(DeliveryLineDescription, SalesOrder.SalesLines.Description.Value, 'Delivery Line Description is correct');
            Assert.AreEqual(DeliveryLineLineAmount, SalesOrder.SalesLines."Line Amount".Value, 'Delivery Line Line Amount is correct');
        end else begin
            Assert.AreNotEqual(DeliveryLineNo, SalesOrder.SalesLines."No.".Value, 'Delivery Line No. not there');
            Assert.AreNotEqual(DeliveryLineDescription, SalesOrder.SalesLines.Description.Value, 'Delivery Line Description not there');
            Assert.AreNotEqual(DeliveryLineLineAmount, SalesOrder.SalesLines."Line Amount".Value, 'Delivery Line Line Amount not there');
        end;
    end;

    local procedure updateQuantity(SalesOrder: TestPage "Sales Order"; lessThan200: Boolean)
    begin
        SalesOrder.SalesLines.First();
        if lessThan200 then begin
            SalesOrder.SalesLines.Quantity.Value := '5';
        end else begin
            SalesOrder.SalesLines.Quantity.Value := '15';
        end;
    end;

    var
        Assert: Codeunit Assert;
        SalesOrderSellToCustomerNo: Code[20];
        SalesLineItemNo: Code[20];
        SalesLineQty1: Label '1'; //for a value < 200;
        SalesLineQty2: Label '10'; //for a value > 200;
        DeliveryLineNo: Label 'DEL-1000';
        DeliveryLineDescription: Label 'Delivery Surcharge';
        DeliveryLineLineAmount: Label '10.00';

}